##========================================
## LeVaDi-Skript 
## for peel crack 
## created by Christian Duereth
## email: christian.duereth@tu-dresden.de
##========================================


from abaqus import *
from abaqusConstants import *
from caeModules import *
from odbAccess import *

import sys
import os


import numpy as np #numpy package of Abaqus is 1.6.2 -- stupid
import math

import json


# function for wrtitting output in console and/or command line
def log(str):
    print >> sys.__stdout__, str
    print(str)

    
##===========================
# Create Part
##===========================
def create_part(para,model,t):
    t_adh   = t
    t_sub   = para["t_sub"]
    l       = para["l"]
    w_load  = para["l"]
    a_0     = para["a_0"]
    w_spec  = para["w_spec"] 
    # create Part
    p1= model.Part(name='part-1',dimensionality=TWO_D_PLANAR,type=DEFORMABLE_BODY)
    # create Sketch
    sketch = model.ConstrainedSketch(name='boundig_box', sheetSize= 200)
    sketch.rectangle(point1=[(-l/2),-(t_adh/2+t_sub)/2], point2=[(l/2),(t_adh/2+t_sub)/2])
    p1.BaseShell(sketch=sketch)
    ##===========================
    ## create partitions in sketch 
    ##===========================
    ## adhesive 
    ##===========================
    trans = p1.MakeSketchTransform(sketchPlane=p1.faces.findAt((0,0,0)) ,
        sketchUpEdge=p1.edges.findAt((l/2,0,0)))
    sketchpartition= model.ConstrainedSketch(name='adhesive', sheetSize=200,transform=trans)
    # Adhesive Layer
    sketchpartition.rectangle(point1=[(-l/2),-(t_adh/2+t_sub)/2], point2=[(l/2),-(t_adh/2+t_sub)/2+(t_adh/2)])
    p1.PartitionFaceBySketch(faces=p1.faces.findAt((0,0,0)), sketch=sketchpartition)
    ##===========================
    ## load 
    ##===========================
    edgesP1 = p1.edges
    facesP1 = p1.faces


    # edge1 =edgesP1.findAt((0,(t_adh/2+t_sub)/2,0))
    # p1.PartitionEdgeByPoint(edge=edge1, point=((-l/2)+w_load,(t_adh/2+t_sub)/2,0))
    ##===========================
    ## initial crack
    ##===========================
    edge2 =edgesP1.findAt((0,-(t_adh/2+t_sub)/2,0))

    p1.PartitionEdgeByPoint(edge=edge2, point=((-l/2)+a_0,-(t_adh/2+t_sub)/2,0))
    model.HomogeneousSolidSection(name='section-adhesive',material='adhesive',
        thickness=w_spec)
    model.HomogeneousSolidSection(name='section-substrate',material='substrate',
        thickness=w_spec)
    ##===========================
    # create sections and Sets
    ##===========================    
    p1.Set(name='set-all',faces=facesP1)

    faceAdh = facesP1.findAt((0,-(t_adh/2+t_sub)/2,0))
    regionAdhesive = p1.Set(name='set-adh',faces=facesP1[faceAdh.index:faceAdh.index+1])
    p1.SectionAssignment(region=regionAdhesive, sectionName='section-adhesive', offset=0.0, 
            offsetType=MIDDLE_SURFACE, offsetField='', thicknessAssignment=FROM_SECTION)


    faceSub = facesP1.findAt((0,(t_adh/2+t_sub)/2,0))
    regionSub = p1.Set(name='set-sub',faces=facesP1[faceSub.index:faceSub.index+1])
    p1.SectionAssignment(region=regionSub, sectionName='section-substrate', offset=0.0, 
            offsetType=MIDDLE_SURFACE, offsetField='', thicknessAssignment=FROM_SECTION)
    
    edge_load = edgesP1.findAt((-l/2+w_load/2,(t_adh/2+t_sub)/2,0))
    p1.Set(name='set-load',edges=edgesP1[edge_load.index:edge_load.index+1])

    edge_initial = edgesP1.findAt((l/2-1e-3,-(t_adh/2+t_sub)/2,0))
    p1.Set(name='set-initial',edges=edgesP1[edge_initial.index:edge_initial.index+1])
    ##===========================
    # Create face(s)
    ##=========================== 
    edge_initial0 = edgesP1.findAt((-l/2,-(t_adh/2+t_sub)/2,0))
    p1.Surface(side1Edges=(edgesP1[edge_initial0.index:edge_initial0.index+1],edgesP1[edge_initial.index:edge_initial.index+1]), name='Surf-contact')

    region = p1.sets['set-all']
    orientation=None
    p1.MaterialOrientation(region=region, 
        orientationType=GLOBAL, axis=AXIS_3, additionalRotationType=ROTATION_NONE, 
        localCsys=None, fieldName='', stackDirection=STACK_3)
##===========================
# Create Material
##===========================
def create_material(para,model):
    #Define the name of two materials
    #model.Material(name='composite')
    model.Material(name='adhesive')
    model.Material(name='substrate')
    #Define the property of materials
    model.materials['substrate'].Elastic(type=ENGINEERING_CONSTANTS,table=para["SubProp"])
    # model.materials['substrate'].Elastic(type=ISOTROPIC,table=para["SubProp"])
    model.materials['adhesive'].Elastic(type=ISOTROPIC,table=para["AdhProp"])
##===========================
# Create Second Mirrored Part
##===========================
def create_mirror(para,model,t):

    t_adh   = t
    t_sub   = para["t_sub"]
    l       = para["l"]
    w_load  = para["l"]
    a_0     = para["a_0"]
    w_spec  = para["w_spec"] 

    p1 = model.parts['part-1']
    p2 = model.Part(name='part-2', 
        objectToCopy=p1, compressFeatureList=ON, 
        mirrorPlane=XZPLANE)

    edgesP2 = p2.edges
    facesP2 = p2.faces

    ##===========================
    # create sections and Sets
    ##===========================    
    p2.Set(name='set-all',faces=facesP2)

    faceAdh = facesP2.findAt((0,(t_adh/2+t_sub)/2,0))
    regionAdhesive = p2.Set(name='set-adh',faces=facesP2[faceAdh.index:faceAdh.index+1])
    p2.SectionAssignment(region=regionAdhesive, sectionName='section-adhesive', offset=0.0, 
             offsetType=MIDDLE_SURFACE, offsetField='', thicknessAssignment=FROM_SECTION)

    faceSub = facesP2.findAt((0,-(t_adh/2+t_sub)/2,0))
    regionSub = p2.Set(name='set-sub',faces=facesP2[faceSub.index:faceSub.index+1])
    p2.SectionAssignment(region=regionSub, sectionName='section-substrate', offset=0.0, 
            offsetType=MIDDLE_SURFACE, offsetField='', thicknessAssignment=FROM_SECTION)
    
    edge_load = edgesP2.findAt((-l/2+w_load/2,-(t_adh/2+t_sub)/2,0))
    p2.Set(name='set-load',edges=edgesP2[edge_load.index:edge_load.index+1])
    edge_initial = edgesP2.findAt((l/2-1E-3,(t_adh/2+t_sub)/2,0))
    p2.Set(name='set-initial',edges=edgesP2[edge_initial.index:edge_initial.index+1])
    ##===========================
    # Create surface(s)
    ##=========================== 
    edge_initial0 = edgesP2.findAt((-l/2*0.9,(t_adh/2+t_sub)/2,0))
    p2.Surface(side1Edges=(edgesP2[edge_initial0.index:edge_initial0.index+1],
        edgesP2[edge_initial.index:edge_initial.index+1]), name='Surf-contact')
    
    region = p2.sets['set-all']
    orientation=None
    p2.MaterialOrientation(region=region, 
        orientationType=GLOBAL, axis=AXIS_3, additionalRotationType=ROTATION_NONE, 
        localCsys=None, fieldName='', stackDirection=STACK_3)
##===========================
# Create Assembly
##===========================
def create_assembly(para,model,t):
    ## Initial parameter
    t_adh   = t
    t_sub   = para["t_sub"]
    l       = para["l"]
    w_load  = para["l"]
    h_load  = para["h_load"]
    w_spec  = para["w_spec"] 
    ## creation of assembly
    assembly = model.rootAssembly
    p1 = model.parts['part-1']
    p2 = model.parts['part-2']
    ##Create a dependent mesh Instance
    assembly.Instance(name='part-1-1', part=p1, dependent=ON)
    assembly.translate(instanceList=('part-1-1', ), vector=(l/2, (t_adh/2+t_sub)/2, 0.0))
    assembly.Instance(name='part-1-2', part=p2, dependent=ON)
    assembly.translate(instanceList=('part-1-2', ), vector=(l/2, -(t_adh/2+t_sub)/2, 0.0))
    ## create datum axis
    assembly.DatumCsysByThreePoints(name='Datum csys-1', coordSysType=CARTESIAN, origin=(
        0.0, 0.0, 0.0), line1=(1.0, 0.0, 0.0), line2=(0.0, 1.0, 0.0))
    ## Create Reference Points
    assembly.ReferencePoint(point=(w_load/2, (t_adh/2+t_sub)/2+h_load/2,0))
    assembly.ReferencePoint(point=(w_load/2,-(t_adh/2+t_sub)/2-h_load/2,0))
    rf = assembly.referencePoints

    ## create sets
    assembly.Set(referencePoints=(rf[6],), name='Set-RP-1')
    assembly.Set(referencePoints=(rf[7],), name='Set-RP-2')
##===========================
# Create Step
##===========================
def create_step(para,model,t):
    ## Define a static step
    model.StaticStep(name='Step-1', previous='Initial', nlgeom=ON, 
        maxNumInc=10000, initialInc=0.0001, maxInc = 0.001, minInc = 1e-12)

    model.steps['Step-1'].setValues(
        stabilizationMagnitude=0.0002, 
        stabilizationMethod=DISSIPATED_ENERGY_FRACTION, 
        continueDampingFactors=False, adaptiveDampingRatio=0.05)
   
    ## Define the outdata
    ## Field Output
    model.fieldOutputRequests['F-Output-1'].setValuesInStep(stepName='Step-1',variables=('S','E','U'),frequency=1)
    ## History Output
    model.HistoryOutputRequest(name='H-Output-1', 
        createStepName='Step-1', variables=('U2', 'RF2'), region=model.rootAssembly.sets['Set-RP-1'], 
        sectionPoints=DEFAULT, rebar=EXCLUDE)
    model.HistoryOutputRequest(name='H-Output-2', createStepName='Step-1', 
        variables=('CAREA', 'ALLIE', 'ALLSE', 'ALLWK', 'ETOTAL','ALLDMD','ALLSD','EFENRRTR','ENRRT11'))
##===========================
# Create Interactions
##===========================
def create_interaction(para,model,t):
    a = model.rootAssembly
    datum = a.datums[5]
    region1=a.sets['Set-RP-1']
    region2=a.instances['part-1-1'].sets['set-load']
   

    model.Coupling(name='Constraint-1', controlPoint=region1, 
        surface=region2, influenceRadius=WHOLE_SURFACE, couplingType=KINEMATIC, 
        localCsys=datum, u1=ON, u2=ON, ur3=ON)

    region1=a.sets['Set-RP-2']
    region2=a.instances['part-1-2'].sets['set-load']
    
    model.Coupling(name='Constraint-2', controlPoint=region1, 
        surface=region2, influenceRadius=WHOLE_SURFACE, couplingType=KINEMATIC, 
        localCsys=datum, u1=ON, u2=ON, ur3=ON)

    ##===========================
    # Create Interaction VCCT
    ##===========================
    ## Create Interaction Properties
    model.ContactProperty('IntProp-1')
    model.interactionProperties['IntProp-1'].TangentialBehavior(
        formulation=PENALTY, directionality=ISOTROPIC, slipRateDependency=OFF, 
        pressureDependency=OFF, temperatureDependency=OFF, dependencies=0, table=((
        0.15, ), ), shearStressLimit=None, maximumElasticSlip=FRACTION, 
        fraction=0.005, elasticSlipStiffness=None)
    model.interactionProperties['IntProp-1'].NormalBehavior(
        pressureOverclosure=HARD, allowSeparation=ON, 
        constraintEnforcementMethod=DEFAULT)
    model.interactionProperties['IntProp-1'].GeometricProperties(contactArea=para["w_spec"], 
        padThickness=None)

    ## have to modify that
    model.interactionProperties['IntProp-1'].FractureCriterion(
        initTable=((para["Gc"], 1.0, 1.0, 1.0), ),
        # viscosity=0.005,
        specifyUnstableCrackProp=False)


    region1     =   a.instances['part-1-1'].surfaces['Surf-contact']
    region2     =   a.instances['part-1-2'].surfaces['Surf-contact']
    regionDef2  =   a.instances['part-1-2'].sets['set-initial']


    model.SurfaceToSurfaceContactStd(name='Int-1', 
        createStepName='Step-1', master=region1, slave=region2, sliding=SMALL, 
        thickness=ON, interactionProperty='IntProp-1', adjustMethod=NONE, datumAxis=None, 
        clearanceRegion=None,
        bondingSet=regionDef2)

    a.engineeringFeatures.DebondVCCT(name='Crack-1', initiationStep='Step-1', 
        surfToSurfInteraction='Int-1')
    
    ##===========================
    # Create Boundary Conditions
    ##===========================
    region = a.sets['Set-RP-1']
    
    model.DisplacementBC(name='BC-1', createStepName='Step-1', 
        region=region, u1=0.0, u2=para["load"], ur3=0, amplitude=UNSET, fixed=OFF, 
        distributionType=UNIFORM, fieldName='', localCsys=datum)

    region = a.sets['Set-RP-2']
    
    model.DisplacementBC(name='BC-2', createStepName='Step-1', 
        region=region, u1=0.0, u2=0, ur3=0, amplitude=UNSET, fixed=OFF, 
        distributionType=UNIFORM, fieldName='', localCsys=datum)
##===========================
##===========================
def create_mesh(para,model):
    meshsize = para["meshsize"] 
    # Set Element types
    elemType1 = mesh.ElemType(elemCode=CPE4, elemLibrary=STANDARD, 
         secondOrderAccuracy=OFF, hourglassControl=DEFAULT, 
         distortionControl=DEFAULT)
    ## Part-1
    p1 = model.parts['part-1']
    p1.seedPart(size=meshsize, deviationFactor=0.1, minSizeFactor=0.1)
    
    p1.seedEdgeBySize(constraint=FINER
        , deviationFactor=0.1, edges=p1.surfaces['Surf-contact'].edges, minSizeFactor=0.1, size=meshsize)
    p1.setElementType(regions=p1.sets['set-all'], elemTypes=(elemType1,))

    p1.generateMesh()

    ## Part-2
    p2 = model.parts['part-2']
    p2.seedPart(size=meshsize, deviationFactor=0.1, minSizeFactor=0.1)
    
    p2.seedEdgeBySize(constraint=FINER
        , deviationFactor=0.1, edges=p2.surfaces['Surf-contact'].edges, minSizeFactor=0.1, size=meshsize)
    p2.setElementType(regions=p2.sets['set-all'], elemTypes=(elemType1,))

    p2.generateMesh()
##===========================
# Create And Submit Job
##===========================
def submit_job(para,model):
    model.rootAssembly.regenerate()

    mdb.Job(name=para["name"], model=para["name"], description='', type=ANALYSIS, 
        atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90, 
        memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
        explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF, 
        modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='', 
        scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=para["cpus"], 
        numDomains=para["cpus"], numGPUs=0)
    mdb.jobs[para["name"]].writeInput(consistencyChecking=OFF)
    mdb.jobs[para["name"]].submit(consistencyChecking=OFF)
    mdb.jobs[para["name"]].waitForCompletion()
##===========================
# Post-Processing
##===========================
def analyse_job(para):
    
    odb = session.openOdb(name= para["name"] +'.odb')
    
    ## History output
    export = {}

    RF2 = session.XYDataFromHistory(
        name='RF2 PI: rootAssembly N: 1 NSET SET-RP-1-1', odb=odb, 
        outputVariableName='Reaction force: RF2 PI: rootAssembly Node 1 in NSET SET-RP-1', 
        steps=('Step-1', ))
    U2 = session.XYDataFromHistory(
        name='U2 PI: rootAssembly N: 1 NSET SET-RP-1-1', odb=odb, 
        outputVariableName='Spatial displacement: U2 PI: rootAssembly Node 1 in NSET SET-RP-1', 
        steps=('Step-1', ))

    export['results'] = {}
    export['results']['step time'] = []
    export['results']['U2_RP-1']   = [] 
    export['results']['RF2_RP-1']  = []
    
    for e1 , e2 in zip(RF2,U2):
        export['results']['step time'].append(e2[0])
        export['results']['U2_RP-1'].append(e2[1]) 
        export['results']['RF2_RP-1'].append(e1[1])

    

    ALLWK = session.XYDataFromHistory(name='ALLWK Whole Model-1', odb=odb, 
        outputVariableName='External work: ALLWK for Whole Model', steps=('Step-1', 
        ))

    ALLIE = session.XYDataFromHistory(name='ALLIE Whole Model-1', odb=odb, 
        outputVariableName='Internal energy: ALLIE for Whole Model', steps=(
        'Step-1', ))

    ALLSE = session.XYDataFromHistory(name='ALLSE Whole Model-1', odb=odb, 
        outputVariableName='Strain energy: ALLSE for Whole Model', steps=('Step-1', 
        ))

    CAREA = session.XYDataFromHistory(
        name='CAREA    ASSEMBLY_PART-1-2_SURF-CONTACT/ASSEMBLY_PART-1-1_SURF-CONTACT-1', 
        odb=odb, 
        outputVariableName='Total area in contact: CAREA    ASSEMBLY_PART-1-2_SURF-CONTACT/ASSEMBLY_PART-1-1_SURF-CONTACT', 
        steps=('Step-1', ))

    ETOTAL = session.XYDataFromHistory(name='ETOTAL Whole Model-1', odb=odb, 
        outputVariableName='Total energy of the output set: ETOTAL for Whole Model', 
        steps=('Step-1', ))


 
    export['results']['ALLWK']     = []
    export['results']['ALLIE']     = []

    export['results']['CAREA']     = []
    export['results']['ETOTAL']    = []

    
    for e1 , e2, e3, e4 in zip(ALLWK,ALLIE,CAREA,ETOTAL):
            export['results']['ALLWK'].append(e1[1])
            export['results']['ALLIE'].append(e2[1])
            export['results']['CAREA'].append(e3[1])
            export['results']['ETOTAL'].append(e4[1])


    export['settings']              = {}
    export['settings']["name"]      = para['name']
    export['settings']["t_sub"]     = para["t_sub"] 
    export['settings']["t_adh"]     = para["t_adh"]
    export['settings']["l"]         = para["l"]   
    export['settings']["w_load"]    = para["l"]  
    export['settings']["h_load"]    = para["h_load"]  
    export['settings']["a_0"]       = para["a_0"]     
    export['settings']["w_spec"]    = para["w_spec"] 
    export['settings']["load"]      = para["load"]    
    export['settings']["AdhProp"]   = para["AdhProp"]
    export['settings']["SubProp"]   = para["SubProp"]
    export['settings']["meshsize"]  = para["meshsize"] 
    export['settings']["Gc"]        = para["Gc"] 
    
    
    
    with open(para['name'] + '.json', 'w') as outfile:
        json.dump(export, outfile)

    
    
##===========================
# MAIN programm
##===========================
def main():

    ##===========================
    # USER INPUT
    ##===========================
    para={}
    para['batch']   = 'batch_01'
    t_adh           = [.1, .2, .3, .4, .5, .6, .7, .8, .9, 1.0,1.5,2.0,3.0,5.0,10.0,20.0 ]
    para["t_sub"]   = 2.     # substrate thickness
    para["l"]       = 80.   # length of DCB specimen
    para["h_load"]  = 15
    para["a_0"]     = 10.    # length of inital crack
    para["w_spec"]  = 1.     # width of specimen
    para["load"]    = .2     # displacement load in mm
    para["Gc"]      = 3.75  # critical energy release rate  
    # linear elastic material
    # engineering constants: E, nu                   
    para["AdhProp"] =[(2.7e3,0.38)]   
    # Substrat Material linear elastic engineering constant
    # engineering constants: E1, E2, E3,  nu12, nu13, nu 23, G12, G13, G23 
    para["SubProp"] =[(39093,13247,13247,0.230,0.230,0.230,3713,3713,5095)]  
    # mesh size is half of the minimum t_adh but at least 0.4
    para["meshsize"]    =   0.25
    ## set the scratch directory
    para["scratch"]     =  os.path.join('scratch-peel',para['batch'])
    ## Settings for calculation
    para["cpus"] = 8



    if not os.path.exists(para["scratch"]):
        os.makedirs(para["scratch"])
        log('created %s' %para["scratch"])
    os.chdir(para["scratch"])
    log('scratch folder %s' %para["scratch"])

    log('##==========START============')
    
    for t in t_adh:
        ## setting up name
        para["name"]    = 'LeVaDi_peel-' + 'tadh_' + str(t).replace('.',',')
        para["t_adh"]   = t

        log('running %s' %para["name"])
     
        model = mdb.Model(para["name"])
        create_material(para,model)
        create_part(para,model,t)
        create_mirror(para,model,t)      
        create_assembly(para,model,t)
        create_step(para,model,t)
        create_interaction(para,model,t)
        create_mesh(para,model)
        submit_job(para,model)
        analyse_job(para)
    log('##========Finished===========')        

if __name__ == "__main__":
    main()