##========================================
## LeVaDi-Skript 
## for normal crack 
## created by Christian Duereth
## email: christian.duereth@tu-dresden.de
##========================================
from abaqus import *
from abaqusConstants import *
from caeModules import *
from odbAccess import *
import sys

import os

import itertools
import numpy as np #numpy package of Abaqus is 1.6.2 -- stupid

import math
import json

##======================================
# auxiliary functions
##======================================
## Function for creating rUC part
def CreatePart(para,model):
    tAdhesive = para["thickness_adhesive"]
    tComposite = para["thickness_composite"]
    t_layer = tComposite/len(para["layer_t"])

    t = 2*tComposite + tAdhesive
    c = para["crack_density"]
    l = 1/c              # length of rUC in [mm]
    # create Part
    part= model.Part(name='part-1',dimensionality=TWO_D_PLANAR,type=DEFORMABLE_BODY)
    # create Sketch
    sketch = model.ConstrainedSketch(name='boundig_box', sheetSize= 200)
    sketch.rectangle(point1=[(-l/2),(-t/2)], point2=[(l/2),(t/2)])
    part.BaseShell(sketch=sketch)
    ##===========================
    ## create partitions in sketch 
    ##===========================
    trans = part.MakeSketchTransform(sketchPlane=part.faces.findAt((0,0,0)) ,
        sketchUpEdge=part.edges.findAt(((l/2),0,0)))
    sketchpartition= model.ConstrainedSketch(name='adhesive', sheetSize=200,transform=trans)
    # Adhesive Layer
    sketchpartition.rectangle(point1=[(-l/2),(-tAdhesive/2)], point2=[(0),(tAdhesive/2)])
    sketchpartition.rectangle(point1=[(0),(-tAdhesive/2)], point2=[(l/2),(tAdhesive/2)])
    # Composite Layers
    for n, layer in enumerate(para["layer_t"]): 
        sketchpartition.rectangle(point1=[(-l/2),(tAdhesive/2+n*t_layer)], 
            point2=[(l/2),(tAdhesive/2+(n+1)*t_layer)])
        sketchpartition.rectangle(point1=[(-l/2),(-(tAdhesive/2+n*t_layer))], 
            point2=[(l/2),(-(tAdhesive/2+(n+1)*t_layer))])

    part.PartitionFaceBySketch(faces=part.faces.findAt((0,0,0)), sketch=sketchpartition)


## Function for creating materials for rUC
def CreateMaterial(para,model):
    #Define the name of two materials
    #model.Material(name='composite')
    model.Material(name='adhesive')
    # model.Material(name='steel')
    #Define the property of materials
    model.materials['adhesive'].Elastic(type=ISOTROPIC,table=para["AdhProp"])
    # create materials for composite Layers
    for n, layer in enumerate(para["layer_t"]):
        model.Material(name='composite_%s' %n)

        stiffness = compositeMaterial(para["SubProp"][0],para["layer_deg"][n])

        ##TODO change to anisotropic and getvalues from fktn "composite material"
        model.materials['composite_%s' %n].Elastic(type=ANISOTROPIC, table=stiffness)
        # model.materials['composite_%s' %n].Elastic(type=ENGINEERING_CONSTANTS,table=para["CompositeProperties"])



def compositeMaterial(properties,deg):
    ##===========================
    # collecting engineering constants
    ##===========================
    # get engineering constant Data
    E1,E2,E3,nu12,nu13,nu23,G12,G13,G23 = properties
    # Pre-Calcualtions
    nu21 = E2/E1*nu12
    nu31 = E3/E1*nu13
    nu32 = E3/E2*nu23
    gamma = 1/(1-nu12*nu21-nu23*nu32-nu31*nu13-2*nu21*nu32*nu13)
    ##===========================
    # Calcualte Stiffness Matrix aniso/orthotropic
    ##===========================
    # orthotropic components
    D1111 = E1*(1.-nu23*nu32)*gamma
    D2222 = E2*(1.-nu13*nu31)*gamma
    D3333 = E3*(1.-nu12*nu21)*gamma
    D1122 = E1*(nu21+nu31*nu23)*gamma
    D1133 = E1*(nu31+nu21*nu32)*gamma
    D2233 = E2*(nu32+nu12*nu31)*gamma
    D1212 = G23
    D1313 = G13
    D2323 = G23
    # anisotrpic components
    D1112 = 0.
    D2212 = 0.
    D3312 = 0.
    D1113 = 0.
    D2213 = 0.
    D3313 = 0.
    D1213 = 0.
    D1123 = 0.
    D2223 = 0.
    D3323 = 0.
    D1223 = 0.
    D1323 = 0.
    ##===========================
    # Transformation Rotation around 1-Axis (90deg); maybe irrelevant for UD
    ##===========================


    ##===========================
    # Transformation Rotation around 3-Axis (arbitary deg)
    ##===========================
    
    return [(D1111,D1122,D2222,D1133,D2233,D3333,D1112,D2212,D3312,D1212,D1113,D2213,D3313,D1213,D1313,D1123,D2223,D3323,D1223,D1323,D2323)]


## Function for creating sections for rUC
def CreateSection(para,model):
    ##===========================
    # create sections
    ##===========================
    tAdhesive = para["thickness_adhesive"]
    tComposite = para["thickness_composite"]
    t = 2*tComposite + tAdhesive
    c = para["crack_density"]
    l = 1/c    # length of rUC in [mm]
    t_layer = tComposite/len(para["layer_t"])       
    #Initial parameter
    part = model.parts['part-1']
    #Define the name of the Section
    for n, layer in enumerate(para["layer_t"]):
        model.HomogeneousSolidSection(name='section-composite_%s' %n,material='composite_%s'%n,
            thickness=para["out-of-plane_thickness"]) 
    model.HomogeneousSolidSection(name='section-adhesive',material='adhesive',
        thickness=para["out-of-plane_thickness"])
    ##===========================
    # assign sections
    ##===========================
    # call faces object
    facesPart = part.faces
    ## set-all for all elements
    part.Set(name='set-all',faces=facesPart)
    #Assign the property of the section-adhesive
    # find faces of adhesive
    locAdhesive=[(-l/4,0,0),(l/4,0,0)]
    faceAdhesive=[]
    for n, loc in enumerate(locAdhesive):
        faceAdhesive.append(facesPart.findAt(loc))
    
    for i, face in enumerate(faceAdhesive):
        name='set-adhesive_' + str(i) 
        regionAdhesive = part.Set(name=name,
            faces=facesPart[face.index:face.index+1])
        part.SectionAssignment(region=regionAdhesive, sectionName='section-adhesive', offset=0.0, 
            offsetType=MIDDLE_SURFACE, offsetField='', thicknessAssignment=FROM_SECTION)
    #Assign the property of the section-composite
    # find faces of composite
    locCompositelayer1 = []
    locCompositelayer2 = []
    for i,  layer in enumerate(para["layer_t"]):
        locCompositelayer1.append([(0,-t/2+t_layer/2+i*t_layer,0)])
        locCompositelayer2.append([(0, t/2-t_layer/2-i*t_layer,0)])

  
    faceCompositelayer1 = []
    faceCompositelayer2 = []
    for loc1,loc2 in zip(locCompositelayer1,locCompositelayer2):
        faceCompositelayer1.append(facesPart.findAt(loc1))
        faceCompositelayer2.append(facesPart.findAt(loc2))

    for i, (face1,face2) in enumerate(zip(faceCompositelayer1,faceCompositelayer2)):
        regionComposite1 = part.Set(name='set-composite_1-%s' %(i) ,faces=face1)
        part.SectionAssignment(region=regionComposite1, sectionName='section-composite_%s' %i, offset=0.0, 
                offsetType=MIDDLE_SURFACE, offsetField='', thicknessAssignment=FROM_SECTION)
        regionComposite2 = part.Set(name='set-composite_2-%s' %(i),faces=face2)
        part.SectionAssignment(region=regionComposite2, sectionName='section-composite_%s' %i, offset=0.0, 
                offsetType=MIDDLE_SURFACE, offsetField='', thicknessAssignment=FROM_SECTION)

    #######
    region = part.sets['set-all']
    orientation=None
    part.MaterialOrientation(region=region, 
        orientationType=GLOBAL, axis=AXIS_3, additionalRotationType=ROTATION_NONE, 
        localCsys=None, fieldName='', stackDirection=STACK_3)


## Function for creating the assembly of the model
def CreateAssembly(para,model):
    ##Initial parameter
    assembly = model.rootAssembly
    part = model.parts['part-1']
    ##Create a in-dependent mesh Instance (independent is necessary for crack definitions)
    assembly.Instance(name='instance-1', part=part, dependent=OFF)
    
## Defining mesh
def CreateStep(para,model):
    #Define a static step
    model.StaticStep(name='Step-1', previous='Initial')

    model.steps['Step-1'].setValues(
        timeIncrementationMethod=FIXED, initialInc=0.1, noStop=OFF)
    #Define the outdata
    model.fieldOutputRequests['F-Output-1'].setValuesInStep(stepName='Step-1',
        variables=('S','E','U'))

    ## History output for whole model    
    model.HistoryOutputRequest(name='H-Output-1', 
        createStepName='Step-1', variables=('ALLAE', 'ALLIE', 'ALLSE', 'ALLWK', 
        'ETOTAL'))







def CreateMesh(para,model):
    log('create '+str(para["elementCode"])+'-mesh with size '+str(para["mesh"]))
    assembly = model.rootAssembly
    instance = (assembly.instances['instance-1'],)
    # Define type of the mesh
    elemType1 = mesh.ElemType(elemCode=para["elementCode"],elemLibrary=STANDARD)
    
    # Assign element type to model
    #assembly.setElementType(regions=, elemTypes=[elemType1])
    # creat mesh seed
    assembly.seedPartInstance(regions=instance, size=para["mesh"], deviationFactor=0.0, 
        minSizeFactor=0.0)
    assembly.setMeshControls(regions=assembly.instances['instance-1'].faces, elemShape=QUAD, technique=STRUCTURED)
    assembly.generateMesh(regions=instance)



## Function for creating the crack 
def CreateCrack(para,model):

    part = model.parts['part-1']
    a = model.rootAssembly
    e1 = a.instances['instance-1'].edges

    edges1 = e1.getByBoundingBox(xMin=(-para["mesh"]), xMax=(para["mesh"]), 
        yMin=(-para["thickness_adhesive"]/2), yMax=(para["thickness_adhesive"]/2), zMin=0, zMax=0)

    a.Set(edges=edges1, name='set-crack')

    a.engineeringFeatures.assignSeam(regions=a.sets['set-crack'])
   
    model.HistoryOutputRequest(createStepName='Step-1', 
        name='H-Output-Crack', rebar=EXCLUDE, region=
        a.sets['set-crack'], 
        sectionPoints=DEFAULT, variables=('U1', ))

def CreateBoundary(para,model):
    a = model.rootAssembly
    Nodeset = a.instances['instance-1'].nodes
    x = []
    y = []
    z = []

    for j, i in enumerate(Nodeset):
        x.insert(j,i.coordinates[0])
        y.insert(j,i.coordinates[1])
        z.insert(j,i.coordinates[2])
        j=j+1
    
    # maximum values
    Max = max(x)
    May = max(y)
    Maz = max(z)
    # minimum values
    Mnx = min(x)
    Mny = min(y)
    Mnz = min(z)

    meshsens = para["mesh"]/2

    ##==============================
    ## create Reference Points
    ##==============================
    a.ReferencePoint(point=(Max+0.8*abs(Max-Mnx), May-0.5*(May-Mny), Maz-0.5*(Maz-Mnz)))  ## RP6: G23
    a.ReferencePoint(point=(Max+0.6*abs(Max-Mnx), May-0.5*(May-Mny), Maz-0.5*(Maz-Mnz)))  ## RP5: G13
    a.ReferencePoint(point=(Max+0.4*abs(Max-Mnx), May-0.5*(May-Mny), Maz-0.5*(Maz-Mnz)))  ## RP4: G12
    a.ReferencePoint(point=(Max+0.2*abs(Max-Mnx), May-0.5*(May-Mny), Maz-0.5*(Maz-Mnz)))  ## RP3: Rigid body movement X-axis
    a.ReferencePoint(point=(Max-0.5*(Max-Mnx), May-0.5*(May-Mny), Maz+0.2*abs(Maz-Mnz)))  ## RP2: Rigid body movement Z-axis
    a.ReferencePoint(point=(Max-0.5*(Max-Mnx), May+0.2*abs(May-Mny), Maz-0.5*(Maz-Mnz)))  ## RP1: Rigid body movement Y-axis
    
    # name Reference Points
    for n, rf in enumerate(a.referencePoints.keys()):
        a.Set(referencePoints=(a.referencePoints[rf], ), name='RP-%s' % (n+1))
        
    corners=[]
    ##==============================
    ## Identifying boundary nodes ##
    ##==============================
    for i in Nodeset:
        if (Mnx+meshsens) < i.coordinates[0] < (Max-meshsens) and (Mny+meshsens) < i.coordinates[1] < (May-meshsens):
            continue
        if abs(i.coordinates[0]-Max)<=meshsens and abs(i.coordinates[1]-May)<=meshsens:
            corners.append((i.label,))
            #coc1[i.label]=[i.coordinates[0], i.coordinates[1]]
        if abs(i.coordinates[0]-Mnx)<=meshsens and abs(i.coordinates[1]-May)<=meshsens:
            corners.append((i.label,))
            #coc2[i.label]=[i.coordinates[0], i.coordinates[1]]
        if abs(i.coordinates[0]-Max)<=meshsens and abs(i.coordinates[1]-Mny)<=meshsens:
            corners.append((i.label,))
            #coc3[i.label]=[i.coordinates[0], i.coordinates[1]]
        if abs(i.coordinates[0]-Mnx)<=meshsens and abs(i.coordinates[1]-Mny)<=meshsens:
            corners.append((i.label,))
            #coc4[i.label]=[i.coordinates[0], i.coordinates[1]]

    ##==============================
    ## Create Sets for PBCs
    ##==============================
    for n , c in enumerate(corners):
        a.SetFromNodeLabels(name='c_%s' % (n+1), nodeLabels=(('instance-1',c),))

 

    ##========================
    ## Identifying edge nodes ##
    edges = []
    edge_1 = []
    edge_2 = []
    edge_3 = []
    edge_4 = []

    # edge_1
    edges.append((Nodeset.getByBoundingBox(xMin=(Mnx+meshsens), xMax=(Max-meshsens), yMin=May-meshsens, yMax=May+meshsens, zMin=0, zMax=0),))
    # edge_2
    edges.append((Nodeset.getByBoundingBox(xMin=(Mnx-meshsens), xMax=(Mnx+meshsens), yMin=Mny+meshsens, yMax=May-meshsens, zMin=0, zMax=0),))
    # edge_3
    edges.append((Nodeset.getByBoundingBox(xMin=(Mnx+meshsens), xMax=(Max-meshsens), yMin=Mny-meshsens, yMax=Mny+meshsens, zMin=0, zMax=0),))
    # edge_4
    edges.append((Nodeset.getByBoundingBox(xMin=(Max-meshsens), xMax=(Max+meshsens), yMin=Mny+meshsens, yMax=May-meshsens, zMin=0, zMax=0),))
    # create sets
    for n, edge in enumerate(edges):
        a.Set(name='edge_%s' % (n+1), nodes = edge)

    ## Sorting and appending sets ##
    for node1 in a.sets['edge_1'].nodes:
        for node2 in a.sets['edge_3'].nodes:
            if abs(node1.coordinates[0]-node2.coordinates[0]) <=meshsens:
                edge_1.append(node1)
                edge_3.append(node2)


    ## Sorting and appending sets ##
    for node1 in a.sets['edge_2'].nodes:
        for node2 in a.sets['edge_4'].nodes:
            if abs(node1.coordinates[1]-node2.coordinates[1]) <=meshsens/4:
                edge_2.append(node1)
                edge_4.append(node2)
  
    


    # Create Sets for oppossing nodes
    for n, (node1,node3) in enumerate(zip(edge_1,edge_3)):
        a.SetFromNodeLabels(name='edge_1-%03d' % n, nodeLabels=(('instance-1',(node3.label,)),))
        a.SetFromNodeLabels(name='edge_3-%03d' % n, nodeLabels=(('instance-1',(node1.label,)),))
        model.Equation(name='x-edge_1-edge_3-%03d'%n, terms=((1.0, 'edge_1-%03d'%n, 1), (-1.0, 'edge_3-%03d'%n, 1)))
        model.Equation(name='y-edge_1-edge_3-%03d'%n, terms=((1.0, 'edge_1-%03d'%n, 2), (-1.0, 'edge_3-%03d'%n, 2),(-1.0, 'RP-5', 2)))


    for n, (node2,node4) in enumerate(zip(edge_2,edge_4)):
        a.SetFromNodeLabels(name='edge_2-%03d' % n, nodeLabels=(('instance-1',(node4.label,)),))
        a.SetFromNodeLabels(name='edge_4-%03d' % n, nodeLabels=(('instance-1',(node2.label,)),))
        model.Equation(name='x-edge_4-edge_2-%03d'%n, terms=((1.0, 'edge_4-%03d'%n, 1), (-1.0, 'edge_2-%03d'%n, 1),(1,'RP-4',1)))
        model.Equation(name='y-edge_2-edge_4-%03d'%n, terms=((1.0, 'edge_4-%03d'%n, 2), (-1.0, 'edge_2-%03d'%n, 2)))


   

    ##========================
    ## Create Equations
    
    
    ## corner constrains in x-direction
    model.Equation(name='x-c32', terms=((1.0, 'c_3', 1), (-1.0, 'c_2', 1)))
    model.Equation(name='x-c21', terms=((1.0, 'c_2', 1), (-1.0, 'c_1', 1),( -1.0, 'RP-4',1)))
    model.Equation(name='x-c14', terms=((1.0, 'c_1', 1), (-1.0, 'c_4', 1)))
    
    
    ## corner constrains in y-Direction                                 
    model.Equation(name='y-c32', terms=((1.0, 'c_3', 2), (-1.0, 'c_2', 2),(1.0, 'RP-5', 2)))
    model.Equation(name='y-c21', terms=((1.0, 'c_2', 2), (-1.0, 'c_1', 2)))
    model.Equation(name='y-c15', terms=((1.0, 'c_1', 2), (-1.0, 'c_4', 2),(-1.0, 'RP-5', 2)))



    region = a.sets['RP-4']
    model.DisplacementBC(name='x-disp', createStepName='Step-1', 
        region=region, u1=para["disp"][0], u2=para["disp"][1], u3=para["disp"][2], 
        ur1=para["disp"][3], ur2=para["disp"][4], ur3=para["disp"][5], 
        amplitude=UNSET, fixed=OFF, distributionType=UNIFORM, fieldName='', 
        localCsys=None)


def CreateJob(para,model):
    ## History output for BC
    ## hast o be here find better seqeuenze of generating RPs
    model.HistoryOutputRequest(name='H-Output-BC', 
        createStepName='Step-1', variables=('U1', 'RF1'), region=model.rootAssembly.sets['RP-4'], 
        sectionPoints=DEFAULT, rebar=EXCLUDE)




    mdb.Job(name=para["name"][-1], model=para["name"][-1], description='', type=ANALYSIS, 
                    atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90, 
                    memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
                    explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF, 
                    modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='', 
                    scratch='', multiprocessingMode=DEFAULT,  numCpus=para["cpus"],
                    numDomains=para["cpus"], numGPUs=0)
    
    
    log('submitting job: '+para["name"][-1])
    mdb.jobs[para["name"][-1]].submit(consistencyChecking=OFF)
    mdb.jobs[para["name"][-1]].waitForCompletion()
    
# Function to export data
def postprocessing(para):

    export = {}
    export['settings'] = {}
    export['settings']["layer_deg"]     =  para["layer_deg"]  
    export['settings']["layer_t"]       =  para["layer_t"]      

    export['settings']["out-of-plane_thickness"]  =  para["out-of-plane_thickness"]   

    export['settings']["strain"]        =  para["strain"]

    export['settings']["mesh"]          =  para["mesh"]
    export['settings']["elementCode"]   =  str(para["elementCode"])

    export['settings']["AdhProp"]  =  para["AdhProp"]
    export['settings']["SubProp"]  =  para["SubProp"]

    export['settings']["crack_density"]  =  para["crack_density"]
    export['settings']["thickness_adhesive"]  =  para["thickness_adhesive"] 
    export['settings']["thickness_composite"]  =    para["thickness_composite"] 
        
    export['settings']["name"]  = para['name']


    for name in para["name"]:
        print(os.path.join(os.getcwd(), name +'.odb'))
        odb = session.openOdb(name=os.path.join(name +'.odb'))
            
        ## History output
 

        U1 = xyPlot.XYDataFromHistory(odb=odb, 
            outputVariableName='Spatial displacement: U1 PI: rootAssembly Node 3 in NSET RP-4', 
            steps=('Step-1', ))
        RF1 = xyPlot.XYDataFromHistory(odb=odb, 
            outputVariableName='Reaction force: RF1 PI: rootAssembly Node 3 in NSET RP-4', 
            steps=('Step-1', ))
        export[name] = {}
        export[name]['results'] = {}
        export[name]['results']['step time'] = []
        export[name]['results']['U1_RP-4']   = [] 
        export[name]['results']['RF1_RP-4']  = []
        export[name]['results']["sigma_x_hom"] = []
        export[name]['results']["eps_x_hom"] = []
        export[name]['results']["C_xx"] = []


        for e1 , e2 in zip(RF1,U1):
            export[name]['results']['step time'].append(e2[0])
            export[name]['results']['U1_RP-4'].append(e2[1]) 
            export[name]['results']['RF1_RP-4'].append(e1[1])
            
            sigma_x_hom = e1[1]/(para["thickness_adhesive"]+para["thickness_composite"])/para["out-of-plane_thickness"]
            eps_x_hom = e2[1]*para["crack_density"]

            export[name]['results']["sigma_x_hom"].append(sigma_x_hom)
            export[name]['results']["eps_x_hom"].append(eps_x_hom)

            export[name]['results']["C_xx"].append(sigma_x_hom/(eps_x_hom + 1e-14))


        ALLWK = session.XYDataFromHistory(name='ALLWK Whole Model-1', odb=odb, 
            outputVariableName='External work: ALLWK for Whole Model', steps=('Step-1', 
            ))

        ALLIE = session.XYDataFromHistory(name='ALLIE Whole Model-1', odb=odb, 
            outputVariableName='Internal energy: ALLIE for Whole Model', steps=(
            'Step-1', ))

        ALLSE = session.XYDataFromHistory(name='ALLSE Whole Model-1', odb=odb, 
            outputVariableName='Strain energy: ALLSE for Whole Model', 
            steps=('Step-1', ))

        ALLAE = session.XYDataFromHistory(name='ALLAE Whole Model-1', odb=odb, 
            outputVariableName='Artificial strain energy: ALLAE for Whole Model', steps=('Step-1', 
            ))

        ETOTAL = session.XYDataFromHistory(name='ETOTAL Whole Model-1', odb=odb, 
            outputVariableName='Total energy of the output set: ETOTAL for Whole Model', 
            steps=('Step-1', ))


        export[name]['results']['ALLWK']     = []
        export[name]['results']['ALLIE']     = []
        export[name]['results']['ALLSE']     = []
        export[name]['results']['ALLAE']     = []
        export[name]['results']['ETOTAL']    = []


        for  e1, e2, e3, e4, e5 in zip(ALLWK,ALLIE,ALLSE,ALLAE,ETOTAL):
            export[name]['results']['ALLWK'].append(e1[1])
            export[name]['results']['ALLIE'].append(e2[1])
            export[name]['results']['ALLSE'].append(e3[1])
            export[name]['results']['ALLAE'].append(e4[1])
            export[name]['results']['ETOTAL'].append(e5[1])


        export['results'] = {}


    for key in ['ALLWK','ALLIE','ALLSE','ALLAE','ETOTAL']:

        keyout = 'd' + key

        x1 = np.array(export[para['name'][1]]['results'][key])
        x2 = np.array(export[para['name'][0]]['results'][key])

        export['results'][keyout] = (x1-x2).tolist()


    with open(os.path.join(para["name"][0] + '.json'), 'w') as outfile:
        json.dump(export, outfile)



# function for wrtitting output in console and/or command line
def log(str):
    print >> sys.__stdout__, str
    print(str)


##===========================
##========Main Program=======
##===========================
def main():
    
    log('##==========START============')
    
   
    ##===========================
    ##=========User Input========
    ##Set parameters and input
    para={}
    para['batch']   = 'batch_01'
    ##===========================
    # Geometry
    ##===========================  
    ##TODO change model preparation    
    para["layer_deg"]   = [0]               # list of layer orientations
    para["layer_t"]     = [1]               # list of layer thicknesses 
    para["FVC"]         = [0.5]             # list of fibervolume content for every layer
    # Fixed parameters for the Computation
    # can be changed accordingly 
    para["out-of-plane_thickness"] = 1   
    ##===========================
    # Boundary Conditions
    ##===========================
    para["strain"]      = 0.01              # applied strain on the model
    ##===========================
    # Mesh
    ##===========================
    para["mesh"]        =   0.05            # Mesh size absolute (should be relative to model-size)
    para["elementCode"] =   CPE8            # An 8-node biquadratic plane strain quadrilateral
    ##===========================
    # Material
    ##===========================
    para["AdhProp"]  =   [(2.7e3,0.38)]
    para["SubProp"] =   [(39093,13247,13247,0.230,0.230,0.230,3713,3713,5095)] 
    ##===========================
    # Set-Up  Geometry for Calculations
    ##===========================
    # Variable Parameters
    # model_c = [0.1,0.2,0.3,0.4,0.5,1.0,2.0]  
    model_c = [1.0,2.0]                        # determines the length [1/cm] -> 1/c == l
    model_t = [.1, .2, .3, .4, .5, .6, .7, .8, .9, 1.0,1.5,2.0,3.0,5.0,10.0,20.0 ]   # thickness of adhesive [mmm]
    ## set the scratch directory
    para["scratch"]     =  os.path.join('scratch-normal',para['batch'])
    ## Settings for calculation
    para["cpus"] = 8

    if not os.path.exists(para["scratch"]):
        os.makedirs(para["scratch"])
        log('created %s' %para["scratch"])
    os.chdir(para["scratch"])
    log('scratch folder %s' %para["scratch"])


    ##===========================
    # Script
    ##===========================
    log('##===========================')
    log('%s models to compute' %(2*len(model_c)*len(model_t)))
    
    for [c , t] in itertools.product(model_c,model_t):
        ##===========================
        # model Crack
        ##===========================
        para["crack_density"] = c       # determines the length [1/cm]
        para["thickness_adhesive"] = t  # thickness of adhesive [mmm]
        para["thickness_composite"] = np.sum(para["layer_t"]) 
        para["name"] = []
        para["name"].append('normal' + '_c' + str(para["crack_density"]).replace('.','-') + '_t' 
            + str(para["thickness_adhesive"]).replace('.','-')) 
        ## set the displacement boundary condition 
        ## set the displacements and rotation [u1,u2,u3,ur1,ur2,ur3]
        para["disp"] = [para["strain"]/c , UNSET , UNSET, UNSET, UNSET, UNSET ]
        log('##===========================')
        log('model: '+para["name"][0])
        log('##===========================')
        model = mdb.Model(para["name"][0])
        CreatePart(para,model)
        CreateMaterial(para,model)
        CreateSection(para,model)
        CreateAssembly(para,model)
        CreateStep(para,model)
        CreateCrack(para,model)
        CreateMesh(para,model)
        CreateBoundary(para,model)
        CreateJob(para,model)
        ##===========================
        # model NO Crack
        ##===========================
        para["name"].append('normal' + '_c' + str(para["crack_density"]).replace('.','-') + '_t' 
            + str(para["thickness_adhesive"]).replace('.','-') + '_noCrack')
        log('##===========================')
        log('model: '+para["name"][1])
        log('##===========================')
        model = mdb.Model(para["name"][1])
        CreatePart(para,model)
        CreateMaterial(para,model)
        CreateSection(para,model)
        CreateAssembly(para,model)
        CreateStep(para,model)
        CreateMesh(para,model)
        CreateBoundary(para,model)
        CreateJob(para,model)

        postprocessing(para)
    log('##===========END=============')
   




if __name__ == "__main__":
    main()